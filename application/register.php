<?php
	require_once("sesion.class.php");

	$sesion = new sesion();
	
	if( isset($_POST["signin"]) ){
		
		$name = $_POST["name"];
		$last_name = $_POST["last_name"];
		$email = $_POST["email"];
		$user = $_POST["user"];
		$password = $_POST["password"];

		
		if(register($name, $last_name, $email, $user, $password) == true){			
			$sesion->set("user",$user);
			header("location: signin.php");
			echo "<script language='javascript'>alert('Su mensaje se ha enviado.');</script>";
		}
	}
	
	function register($name, $last_name, $email, $user, $password){
		$conexion = new mysqli("localhost","root","root","usuarios");
		$consulta = "insert into usuarios (name, last_name, email, user, password) values ('$name', '$last_name', '$email', '$user', '$password')" ;
		
		$result = $conexion->query($consulta);
		
		return ($result > 0);
	}

?>
<html>
<head>
	<!--title-->
	<title>TEST - Web Developer - PHP</title>
	<!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="Dulce Soto Juáres">
	<meta name="description" content="website for user registration, login and password change.">
	<!-- Mobile Specific Metas-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!--CSS-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
	<section class="background">
		<div class="container padre">
			<form name="frmRegister" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-signin hijo">
			  <div>
			   	<div class="form-group">
			   		<input type="text" class="form-control" name="name" placeholder="Name" required/>
			   	</div>
			    <div class="form-group">
			    	<input type="text" class="form-control" name="last_name" placeholder="Last Name" required/>
			    </div>
			    <div class="form-group">
			    	<input type="email" class="form-control" name="email" placeholder="Email" required/>
			    </div>
			    <div class="form-group">
			    	<input type="text" class="form-control" name="user" placeholder="User" required/>
			    </div>
			    <div class="form-group">
			    	<input type="password" class="form-control" name="password" placeholder="Password" required/>
			    </div>
			    <div class="form-group"><input type="submit" name ="signin" value="Sign in"/></div>
			    <div>
			    	<a href="register.php">are you a new user? Sign up</a>
			    </div>
			  </div>
			</form>
		</div>
	</section>
</body>
</html>

