<?php
	require_once("sesion.class.php");

	$sesion = new sesion();
	
	if( isset($_POST["iniciar"]) ){
		
		$usuario = $_POST["usuario"];
		$password = $_POST["password"];
		
		if(validarUsuario($usuario,$password) == true){			
			$sesion->set("usuario",$usuario);
			
			header("location: welcome.php");
		}
		else {
			echo "Verifica tu nombre de usuario y contrase�a";
		}
	}
	
	function validarUsuario($usuario, $password){
		$conexion = new mysqli("localhost","root","root","usuarios");
		$consulta = "select password from usuarios where user = '$usuario';";
		
		$result = $conexion->query($consulta);
		
		if($result->num_rows > 0){
			$fila = $result->fetch_assoc();
			if( strcmp($password,$fila["password"]) == 0 )
				return true;						
			else					
				return false;
		}
		else
				return false;
	}

?>
<html>
<head>
	<!--title-->
	<title>TEST - Web Developer - PHP</title>
	<!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="Dulce Soto Juáres">
	<meta name="description" content="website for user registration, login and password change.">
	<!-- Mobile Specific Metas-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!--CSS-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
	<section class="background">
		<div class="container padre">
			<form name="frmLogin" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-signin hijo">
			  <div>
			   <div class="form-group">
			   		<input type="text" class="form-control" name="usuario" placeholder="User" required/>
			   	</div>
			    <div class="form-group">
			    	<input type="password" class="form-control" name="password" placeholder="Password" required/>
			    </div>
			    <div>
					<a href="">Forgot password?</a>
				</div>
			    <div class="form-group"><input type="submit" name ="iniciar" value="Iniciar Sesion"/></div>
			    <div>
			    	<a href="register.php">are you a new user? Sign up</a>
			    </div>
			  </div>
			</form>
		</div>
	</section>
</body>
</html>

