<?php
	require_once("sesion.class.php");
	
	$sesion = new sesion();
	$usuario = $sesion->get("usuario");
	
	if( $usuario == false )
	{	
		header("Location: login.php");		
	}
	else 
	{
	?>
	<html>
<head>
	<!--title-->
	<title>TEST - Web Developer - PHP</title>
	<!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="Dulce Soto Juáres">
	<meta name="description" content="website for user registration, login and password change.">
	<!-- Mobile Specific Metas-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!--CSS-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/fontawesome.min.css">
	<link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
	<section class="background">
		<div class="container padre">
			<div class="hijo">
				<h1>Welcome!: <?php echo $sesion->get("usuario");?><i class="fa fa-user-circle" aria-hidden="true"></i></h1>
				<a href="logout.php"> Logout </a>
			</div>
		</div>
	</section>
</body>
</html>
	
	<?php 
	}	
?>